<?php
 
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $email = $_POST['email'];
 $pwd = $_POST['password'];

    echo "your emial: ". $email . "and your password " . $pwd . " is successfully submited";
}
?>
<!Doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--================================ [Bootstrap CSS] ========================================-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/custom-animation.css">
    <!-- Wow Animate.csss -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h1>Login Form</h1>
                <form action="postmethod.php" method="post">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" name="email" id="" aria-describedby="emailHelpId" placeholder="">
                        <small id="emailHelpId" class="form-text text-muted">Help text</small>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" class="form-control" name="password" id="" placeholder="">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================ [JQuery, Bootstrap 4 CSS] ============================-->
    <script src="js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawsome.js"></script>
    <!-- ================================= [Custom.js] ========================================-->
    <script src="js/custom.js"></script>
    <!-- ================================= [WOW.js] ========================================-->
    <script src="js/wow.min.js"></script>
</body>

</html>
