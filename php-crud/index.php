<?php
$servername="localhost";
$username="root";
$password="";
$database="mynotes";
$conn = mysqli_connect($servername,$username,$password,$database);
$insert = false;
$update =false;
$delete=false;
// delete
if (isset($_GET['delete'])){
  $notes_id=$_GET['delete'];
  $sql= "DELETE FROM notes WHERE notes.notes_id = $notes_id";
  $request = mysqli_query($conn,$sql);
  if($request){
    $delete= true;
  }
  else{
     echo "sorry record not update" . mysqli_error($conn);
  }
}
// insert and  update
if($_SERVER['REQUEST_METHOD'] == "POST"){
  if(isset($_POST['snoEdit'])){
// update
$sno=$_POST['snoEdit'];
$title = $_POST['titleEdit'];
$desc = $_POST['descEdit'];

$sql= "UPDATE `notes` SET `title`='$title',`detail`='$desc' WHERE `notes`.`notes_id`= $sno";
$request = mysqli_query($conn,$sql);

if($request){
  $update= true;
}
else{
   echo "sorry record not update" . mysqli_error($conn);
}
  }else{
    $title = $_POST['title'];
    $desc = $_POST['desc'];

 
 $sql= "INSERT INTO notes (title, detail)  VALUES ('$title', '$desc')";
 $request = mysqli_query($conn,$sql);
 
  if($request){
     $insert = true;
  }
  else{
    echo "sorry record not inserted" . mysqli_error($conn);
       
  }
  }

 
}
?>
<!Doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--================================ [Bootstrap CSS] ========================================-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/custom-animation.css">
    <!-- Wow Animate.csss -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/style.css">
    <!-- ================================= [Data Tables] ========================================-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<?php
if($insert){
    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
    <strong>Successfully</strong>.Insert Record In Table
    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
      <span aria-hidden='true'>&times;</span>
    </button>
  </div>";
} 
elseif($update){
  echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
  <strong>Successfully</strong>.Update  Record In Table
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button>
</div>";
}
elseif($delete){
  echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
  <strong>Successfully</strong>.Deleted  Record In Table
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button>
</div>";
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <h1>My Note</h1>
                <form action="index.php" method="post">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="title" class="form-control" name="title" id="title" aria-describedby="emailHelpId" placeholder="Notes Title" required>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="desc" id="desc" rows="3" placeholder="description" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <!-- data table -->
    <div class="container">
    <div class="col-md-10 mx-auto mt-5">
    <h1 class="w-100">Records</h1>
        <table class="table   mx-auto" id="myTable">
            <thead>
                <tr>
                    <th>Sno.</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
           
            <tbody>
                <tr>
                <?php 
                $sql = "SELECT * FROM notes";
                $result = mysqli_query($conn,$sql);
                $Sno = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $Sno++;
                  echo "
                  <tr>
                    <th>$Sno</th>
                    <td class='editTitle'>$row[title]</td>
                    <td class='editDetail'>$row[detail]</td>
                    <td>        
                    <button type='button' id=' $row[notes_id] ' class='edit btn btn-primary' >Edit</button> 
                    <button type='button' id=d'$row[notes_id]'  class='delete btn btn-primary'>delete</button>
                    </td> 
                  </tr> 
                 "; 
                }
                  
                ?>
                </tr>
            </tbody>
        </table>
    </div>
  
    </div>
    <!-- edit modal -->
    <!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editLabel">Edit Notes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="index.php" method="post">
      <input type="hidden" name="snoEdit" id="snoEdit">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="title" class="form-control" name="titleEdit" id="titleEdit" aria-describedby="emailHelpId" placeholder="Notes Title" required>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="descEdit" id="descEdit" rows="3" placeholder="description" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    <!-- ============================ [JQuery, Bootstrap 4 CSS] ============================-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawsome.js"></script>
    <!-- ================================= [Custom.js] ========================================-->
    <script src="js/custom.js"></script>
    <!-- ================================= [WOW.js] ========================================-->
    <script src="js/wow.min.js"></script>
    <!-- ================================= [Data Tables] ========================================-->
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready( function () {
        $('#myTable').DataTable();
        
    });
    </script>
    <script>
    // edit script
     edits = document.getElementsByClassName("edit");
    Array.from(edits).forEach((element)=>{
      element.addEventListener("click", (e)=>{
       tr= e.target.parentNode.parentNode;
       title=tr.getElementsByTagName("td")[0].innerText;
       desc=tr.getElementsByTagName("td")[1].innerText;
       snoEdit.value=e.target.id;
       titleEdit.value=title;
       descEdit.value=desc;
    
        $("#editModal").modal('toggle');
      });
    })

    // delete script
    deletes = document.getElementsByClassName("delete");
    Array.from(deletes).forEach((element)=>{
      element.addEventListener("click", (e)=>{
     
       snoDelete=e.target.id.substr(1,);
      
    if (confirm("press a button")){
       
      window.location =`/php-learning/php-crud/index.php?delete=${snoDelete}`
    }else{
      alert("no")
    }
     
      });
    })
    </script>
</body>

</html>
