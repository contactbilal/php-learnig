<!Doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--================================ [Bootstrap CSS] ========================================-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/custom-animation.css">
    <!-- Wow Animate.csss -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- ================================= [Main CSS] ========================================-->
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h1>Pages</h1>
                 <div class="list-group ">
                     <a href="postmethod.php" class="list-group-item list-group-item-action active">Post</a>
                     <a href="dbConnection.php" class="list-group-item list-group-item-action">DbConnection</a>
                     <a href="createTbl.php" class="list-group-item list-group-item-action  ">Createtable</a>
                     <a href="updatedbTbl.php" class="list-group-item list-group-item-action">Updatetable</a>
                     <a href="select-displayRecod.php" class="list-group-item list-group-item-action">select-displayRecod </a>
                     <a href="php-crud/index.php" class="list-group-item list-group-item-action">Php Crud</a>
                 </div>
            </div>
        </div>
    </div>
    <!-- ============================ [JQuery, Bootstrap 4 CSS] ============================-->
    <script src="js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fontawsome.js"></script>
    <!-- ================================= [Custom.js] ========================================-->
    <script src="js/custom.js"></script>
    <!-- ================================= [WOW.js] ========================================-->
    <script src="js/wow.min.js"></script>
</body>

</html>
